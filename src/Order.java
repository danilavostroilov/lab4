public class Order {
    private int id;
    private int time;
    private String date;


    public Order (int id, int time, String date) {
        this.id = id;
        this.time = time;
        this.date = date;

    }

    public String toString() {
        return super.toString()+"\n"+
                "id=" + id + "\n" +
                "time=" + time + "\n" +
                "date=" + date;

    }
}

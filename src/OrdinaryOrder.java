public class OrdinaryOrder extends Order {
    private int id;
    private int time;
    private String date;


    public OrdinaryOrder (int id, int time, String date) {
        super (id,time,date);
        this.id = id;
        this.time  = time;
        this.date = date;

    }

    public String toString() {
        return super.toString()+"\n"+
                "id=" + id + "\n" +
                "time=" + time + "\n" +
                "date=" + date;

    }
}


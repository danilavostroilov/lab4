import java.util.ArrayList;
public class Main {
    public static void main(String[] args) {
        ArrayList<Order> orderArrayList = new ArrayList<Order>();

        OrdinaryOrder ordinaryOrder =new OrdinaryOrder(1, 19, "8.11.2020");
        orderArrayList.add(ordinaryOrder);

        ExpressOrder expressOrder=new ExpressOrder(2,18,"11.11.2020", "Андрей", "10.12.2020");
        orderArrayList.add(expressOrder);
        InsuredOrder insuredOrder=new InsuredOrder(1,12,"12.12.2020","компания 1", "2000");
        orderArrayList.add(insuredOrder);
        for(Order order: orderArrayList){
            System.out.println(order + "\n");
        }
    }


}



public class InsuredOrder extends Order {
    private String company;
    private String amount;

    public InsuredOrder (int id, int time, String date, String company ,String amount) {
        super (id,time,date);
        this.company = company;
        this.amount =amount;
    }



    public String toString() {
        return super.toString()+"\n"+
                "company=" + company + "\n" +
                "amount=" + amount;
    }

}

public class ExpressOrder extends Order {
    private String courier;
    private String daysToDeliver;

    public ExpressOrder (int id, int time, String  date,  String courier ,String daysToDeliver ) {
        super (id,time,date);
        this.courier = courier;
        this.daysToDeliver = daysToDeliver;
    }


    @Override
    public String toString() {
        return super.toString()+"\n"+
                "courier=" + courier + "\n" +
                "daysToDeliver=" + daysToDeliver;
    }
}

